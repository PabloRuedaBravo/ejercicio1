#include "ros/ros.h"
#include "std_msgs/String.h"

void funcion_datos(const std_msgs::String::ConstPtr& datos)
{
	std_msgs::String aux;
	std_msgs::String name;
	std_msgs::String age;
	std_msgs::String height;

	/*std::stringstream posi;
	std::stringstream posi2;

	std_msgs::String pos;
	std_msgs::String pos2;*/

	int posicion;
	int posicion2;
	int posicion3;

	aux.data=datos->data.c_str();
	
	posicion=aux.data.find("name");
	posicion2=aux.data.find("age");
	posicion3=aux.data.find("height");

	name.data=aux.data.substr(posicion+6,posicion2-8);
	age.data=aux.data.substr(posicion2+5,posicion3-19);
	height.data=aux.data.substr(posicion3+8,aux.data.size());

	/*posi<<posicion;
	posi2<<posicion2;

	pos.data=posi.str();
	pos2.data=posi2.str();*/

	//ROS_INFO(aux.data.c_str());
	ROS_INFO("Name: %s",name.data.c_str());
	ROS_INFO("Age: %s",age.data.c_str());
	ROS_INFO("Height: %s",height.data.c_str());
}
int main(int argc, char **argv)
{
	ros::init(argc, argv, "data_processing");
	
	ros::NodeHandle n;

	ros::Subscriber sub = n.subscribe("/raw_data", 1000, funcion_datos);

	ros::spin();

	return 0;
}
